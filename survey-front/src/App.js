import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { createBrowserHistory } from "history";
import './App.css';

import Navbar from "./components/Navbar";
import routes from './shared/routes';
import RouteWithSubRoutes from "./shared/components/RouteWithSubRoutes";

const history = createBrowserHistory();

export default function App() {
    return (
        <Router history={history}>
            <div>
                <Navbar/>
                <Switch>
                    {routes.map((route, i) => (
                        <RouteWithSubRoutes key={i} {...route} />
                    ))}
                </Switch>
            </div>
        </Router>
    );
}


