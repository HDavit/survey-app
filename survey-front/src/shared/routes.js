import AboutRoute from "../screens/About/route";
import HomeRoute from "../screens/Home/route";
import ReportRoute from "../screens/Report/route";
import SurveyRoute from "../screens/Survey/route";
import ThankYouRoute from "../screens/ThankYou/route";

export default [
    AboutRoute,
    HomeRoute,
    ReportRoute,
    SurveyRoute,
    ThankYouRoute
];
