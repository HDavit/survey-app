export default Object.freeze({
    HOME: {
        name: 'Home',
        route: '/'
    },
    ABOUT: {
        name: 'About',
        route: '/about'
    },
    REPORT: {
        name: 'Report',
        route: '/report'
    },
    SURVEY: {
        name: 'Survey',
        route: '/survey'
    },
    THANK_YOU: {
        name: 'ThankYou',
        route: '/thank_you'
    },
});
