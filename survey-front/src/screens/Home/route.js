import {routeConstants} from "../../shared/constants";
import Home from "./index";

export default {
    path: routeConstants.ABOUT.route,
    component: Home
};
