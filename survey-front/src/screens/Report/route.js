import {routeConstants} from "../../shared/constants";
import Report from "./index";

export default {
    path: routeConstants.REPORT.route,
    component: Report
};
