import React,{ useEffect, useState } from 'react';
import { Spin, Statistic, Row, Col, Button } from "antd";

import {SURVEY_SERVICE_URL} from "../../../config";

const renderDistribution = data => data.map(distributionItem => Object.entries(distributionItem)
    .map(([key, value]) => {
        return (
            <Col span={4}>
                <Statistic title={key} value={value} />
            </Col>
        )
    })
)

const Report = () => {
    useEffect(() => {
        fetch(`${SURVEY_SERVICE_URL}/report`)
            .then((response) => {
                if(response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(data => console.log(data) || setData(data))
            .catch((err) => setError(['Something went wrong']))
    }, [])

    const [data, setData] = useState(null);
    const [err, setError] = useState(null);


   return (
       <div className="report">
           <h2>
               Report Page
           </h2>
           { err ? <div>Error</div> :
               !data ? <Spin/>
               : <div className="report-statistics">
                   <Row gutter={16}>
                       <Col span={4}>
                           <Statistic title="Number of respondents" value={data.count} />
                       </Col>
                       <Col span={4}>
                           <Statistic title="Average Age" value={data.averageAge} />
                       </Col>
                       <Col span={4}>
                           <Statistic title="Average Experience" value={data.averageExperience} />
                       </Col>


                   </Row>

                   <div className="report-statistics-countries">
                       <div className="report-statistics-countries-title">Country Distribution</div>
                       <Row gutter={16}>
                           {data.countryDistribution.length ? renderDistribution(data.countryDistribution) : <Statistic title="Country Distribution" value={0} /> }
                       </Row>
                   </div>


                   <div className="report-statistics-genders">
                       <div className="report-statistics-genders-title">Gender Distribution</div>
                       <Row gutter={16}>
                           {data.genderDistribution.length ? renderDistribution(data.genderDistribution) : <Statistic title="Gender Distribution" value={0} /> }
                       </Row>
                   </div>

                </div>
           }

       </div>
   )
}

export default Report;
