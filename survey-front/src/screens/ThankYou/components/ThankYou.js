import React from "react";
import {Button} from "antd";


const goBack = (props) => {
    props.history.go(-2)
}

const ThankYou = (props) => (
    <React.Fragment>
        <h2>Thank you for filling the from</h2>;
        <Button type="primary" onClick={() => goBack(props)}>Go Back</Button>
    </React.Fragment>
)

export default ThankYou
