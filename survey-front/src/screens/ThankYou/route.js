import {routeConstants} from "../../shared/constants";
import ThankYou from "./index";

export default {
    path: routeConstants.THANK_YOU.route,
    component: ThankYou
};
