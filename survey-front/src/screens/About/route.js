import {routeConstants} from "../../shared/constants";
import About from "./index";

export default {
    path: routeConstants.ABOUT.route,
    component: About
};
