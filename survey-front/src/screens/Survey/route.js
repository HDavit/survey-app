import {routeConstants} from "../../shared/constants";
import Survey from "./index";

export default {
    path: routeConstants.SURVEY.route,
    component: Survey
};
