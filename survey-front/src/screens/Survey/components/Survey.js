import React, { useState } from 'react';
import { Form, Input, Button, Rate, Select, InputNumber,  } from 'antd';
import {SURVEY_SERVICE_URL} from "../../../config";
import countryCodes from '../countries';

const { Option } = Select;
const { TextArea } = Input;
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};


const Survey = (props) => {
    const [err, setError] = useState(null);
    const onFinish = (values) => {
        (async () => {

            try {
                const response = await fetch(`${SURVEY_SERVICE_URL}/survey`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        ...values,
                        ...(document.referrer ? {originPage: document.referrer} : {})
                    })
                });
                await response.json();
                setError(null)
                props.history.push('thank_you')

            } catch (err){
                setError({msg: 'Unexpected Error'})
            }

        })()
    };

    return (
        <React.Fragment>
            <div className="survey-form">
            <Form
                {...layout}
                name="basic"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Name"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your name!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item name="age" label="Age">
                    <InputNumber type="number" />
                </Form.Item>

                <Form.Item name="gender" label="Gender">
                    <Select>
                        <Option value="male">male</Option>
                        <Option value="female">female</Option>
                    </Select>
                </Form.Item>

                <Form.Item name="country" label="Country">
                    <Select>
                        {countryCodes.map(countryCode => <Option value={countryCode}>{countryCode}</Option>)}
                    </Select>
                </Form.Item>


                <Form.Item name="suggestion" label="Suggestions">
                    <TextArea />
                </Form.Item>


                <Form.Item name="experience" label="Experience">
                    <Rate />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
        <span className="survey-form-error">{err && err.msg}</span>
        </React.Fragment>
    );
};

export default Survey
