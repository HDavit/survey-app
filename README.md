# To run the backend part:

### cd survey-backend
### docker-compose up --build

# To run the frontend part:

### cd survey-front
### npm run start
