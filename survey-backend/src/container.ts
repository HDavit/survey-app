import 'reflect-metadata';
import { Container, interfaces } from 'inversify';

import TYPES from 'core/types';
import Config from 'config';

// enums
import Timer from 'core/interfaces/timer';
import RequestTimer from 'infra/requestTimer';

// System interfaces
import { Log } from 'core/interfaces/log';
import PresentationLayer from 'core/interfaces/presentationLayer';
import { HttpClient } from 'core/interfaces/clients/http';

// System implementations
import AxiosHttpClient from 'infra/axiosHttpClient';
import BunyanLog from 'infra/log/bunyan';

import { scopePerRequest } from 'presentation/rest/plugins/inversifyHapi';
import Server from 'presentation/rest/server';

import CreateSurvey from 'app/survey/create';
import GetReport from 'app/survey/getReport';
import DatabaseClient from 'infra/modules/dbClient';
// Instrumentation

import CreateSurveyInstrumentation from 'app/instrumentation/survey/create';
import GetReportInstrumentation from 'app/instrumentation/survey/getReport';
import Application from 'app/application';
import InsertSurvey from 'infra/repositories/survey/insert';
import GenerateReport from 'infra/repositories/survey/report';

const container = new Container();

// System
container
    .bind<Config>(TYPES.Config)
    .to(Config)
    .inSingletonScope();

container
    .bind<Application>(TYPES.Application)
    .to(Application)
    .inSingletonScope();

container
    .bind<PresentationLayer>(TYPES.PresentationLayer)
    .to(Server)
    .inSingletonScope();

container.bind(TYPES.InversifyHapi).toConstantValue(scopePerRequest(container));

container.bind<Log>(TYPES.Log).to(BunyanLog);

container.bind<interfaces.Factory<Log>>(TYPES.LogFactory).toFactory<Log>((context: interfaces.Context) => {
    return (ctx: string) => {
        return context.container.get<Log>(TYPES.Log).setContext(ctx);
    };
});

// Application
container.bind<DatabaseClient>(TYPES.DatabaseClient)
    .to(DatabaseClient).inSingletonScope();

container.bind<InsertSurvey>(TYPES.InsertSurvey)
    .to(InsertSurvey).inTransientScope();

container.bind<GenerateReport>(TYPES.GenerateReport)
    .to(GenerateReport).inTransientScope();

container.bind<CreateSurvey>(TYPES.CreateSurvey)
    .to(CreateSurvey).inTransientScope();

container.bind<GetReport>(TYPES.GetReport)
    .to(GetReport).inTransientScope();

// Instrumentation
container.bind<CreateSurveyInstrumentation>(TYPES.CreateSurveyInstrumentation)
    .to(CreateSurveyInstrumentation)
    .inTransientScope();

container.bind<GetReportInstrumentation>(TYPES.GetReportInstrumentation)
    .to(GetReportInstrumentation)
    .inTransientScope();

container.bind<HttpClient>(TYPES.HttpClient).to(AxiosHttpClient);
container.bind<Timer>(TYPES.Timer).to(RequestTimer);

// module export
export default container;
