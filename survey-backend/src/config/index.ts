import * as convict from 'convict';
import * as path from 'path';
import * as fs from 'fs';
import { injectable } from 'inversify';

import Env from 'core/enums/env';
import LogLevel, { LogType } from 'core/enums/logLevel';
import * as JSYaml from 'js-yaml';

@injectable()
export default class Config {
    private readonly convict;

    /* eslint-disable max-lines-per-function */
    constructor() {
        convict.addParser({ extension: 'yaml', parse: JSYaml.load });
        this.convict = convict({
            env: {
                default: null,
                doc: 'The application environment',
                env: 'RUNTIME_ENV',
                format: [Env.Production, Env.Testing, Env.Development, Env.Sandbox, Env.Staging],
            },
            http: {
                host: {
                    default: null,
                    doc: 'Application host on which it is running',
                    env: 'HTTP_HOST',
                    format: String,
                },
                port: {
                    default: null,
                    doc: 'Application host port',
                    env: 'HTTP_PORT',
                    format: 'port',
                },
                timeout: {
                    default: null,
                    doc: 'Max Timeout for services being called in ms',
                    env: 'HTTP_TIMEOUT',
                    format: Number,
                },
            },
            db: {
                host: {
                    default: null,
                    doc: 'Database Host',
                    env: 'DB_HOST',
                    format: String,
                },
                name: {
                    default: null,
                    doc: 'Database name',
                    env: 'DB_NAME',
                    format: String,
                },
                user: {
                    default: null,
                    doc: 'Database user',
                    env: 'DB_USER',
                    format: String,
                },
                port: {
                    default: null,
                    doc: 'Database port',
                    env: 'DB_PORT',
                    format: Number,
                },
                password: {
                    default: null,
                    doc: 'Database password',
                    env: 'DB_PASSWORD',
                    format: String,
                },
            },
            log: {
                level: {
                    default: null,
                    doc: 'Granularity of logs to show',
                    env: 'LOG_LEVEL',
                    format: [LogLevel.Error, LogLevel.Warn, LogLevel.Info, LogLevel.Debug, LogLevel.Trace],
                },
                masks: {
                    default: '',
                    format: String,
                    env: 'LOG_MASKS',
                },
                type: {
                    default: LogType.Stream,
                    env: 'LOG_TYPE',
                    format: [
                        LogType.Stream,
                        LogType.File,
                        LogType.RotatingFile,
                    ],
                },
                filePath: {
                    default: 'main.log',
                    env: 'LOG_FILE_PATH',
                    format: String,
                },
                rotatingPeriod: {
                    env: 'LOG_ROTATING_PERIOD',
                    default: '',
                    format: String,
                },
                rotatingCount: {
                    env: 'LOG_ROTATING_COUNT',
                    default: 0,
                    format: Number,
                },
            },
            name: {
                default: null,
                doc: 'Name of the application.',
                env: 'NAME',
                format: String,
            },
            restPresentation: {
                maxLimit: {
                    default: null,
                    doc: 'Maximum number of result in list API if limit is not specified.',
                    env: 'REST_PRESENTATION_MAX_LIMIT',
                    format: Number,
                },
            },
        });
        const configurationDir = path.join(process.cwd(), 'assets/config');
        this.convict.loadFile(path.join(configurationDir, 'default.yaml'));
        if (!process.env.RUNTIME_ENV) {
            throw Error(
                'Environment variable RUNTIME_ENV is needed to start the application.'
                + `Accepted Values: [${[Env.Production, Env.Testing, Env.Development, Env.Sandbox, Env.Staging]}]`,
            );
        }
        this.convict.loadFile(path.join(configurationDir, `${process.env.RUNTIME_ENV}.yaml`));
        if (
            process.env.NODE_CONFIG_FILE
        ) {
            const overrideConfigFile = process.env.NODE_CONFIG_FILE;
            if (fs.existsSync(overrideConfigFile)) {
                this.convict.loadFile(overrideConfigFile);
            } else {
                throw Error('The NODE_CONFIG_FILE path does not exists, please create a config file.');
            }
        }
        this.convict.validate({
            allowed: 'strict',
        });
    }

    public get name(): string {
        return this.convict.get('name');
    }

    public get env(): Env {
        return this.convict.get('env');
    }

    public get log(): {
        level: LogLevel;
        masks: string[];
        type?: LogType;
        path?: string;
        period?: string;
        count?: number;
    } {
        const masks = this.convict.get('log.masks')
            ? this.convict.get('log.masks').split(',')
            : [];
        return {
            level: this.convict.get('log.level'),
            masks,
            type: this.convict.get('log.type'),
            path: this.convict.get('log.filePath'),
            period: this.convict.get('log.rotatingPeriod'),
            count: this.convict.get('log.rotatingCount'),
        };
    }

    public get http(): {
        host: string;
        port: number;
        timeout: number;
    } {
        return {
            host: this.convict.get('http.host'),
            port: this.convict.get('http.port'),
            timeout: this.convict.get('http.timeout'),
        };
    }

    public get db(): {
        host: string;
        name: string;
        user: string;
        port: number;
        password: string;
    } {
        return {
            host: this.convict.get('db.host'),
            name: this.convict.get('db.name'),
            user: this.convict.get('db.user'),
            port: this.convict.get('db.port'),
            password: this.convict.get('db.password'),
        };
    }

    public get rest(): {
        maxLimit: number;
    } {
        return {
            maxLimit: this.convict.get('restPresentation.maxLimit'),
        };
    }
}
