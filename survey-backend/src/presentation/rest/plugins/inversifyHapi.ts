import { PluginBase, Server } from '@hapi/hapi';
import { Container } from 'inversify';

export default class InversifyHapi implements PluginBase<any> {
    public readonly name: string = 'inversify-hapi';
    public readonly version: string = '1.0.0';
    private readonly container: Container;

    constructor(container: Container) {
        this.container = container;
    }

    public async register(server: Server): Promise<void> {
        server.decorate('request', 'container', () => this.container, { apply: true });
    }
}

export function scopePerRequest(container: Container): InversifyHapi {
    return new InversifyHapi(container);
}

/**
 * Builds a hapi route prerequisite object that resolves a requested dependency,
 * adding it to an array
 *
 * @param pre
 * @param dependency
 */
// TODO: Fix any
function buildDependencyPrerequisites(pre, dependency): any {
    pre.push({
        assign: dependency.toString(),
        method({ container }) {
            return container.get(dependency);
        },
    });
    return pre;
}

/**
 * Returns a set of hapi route prerequisite objects that resolve the requested
 * dependencies
 *
 * inject('name1'[, 'name2' [,'name3' ...]])
 *
 * @param dependencies string A list of dependency names
 */
// TODO: Fix any
export function inject(...dependencies): any {
    return dependencies.reduce(buildDependencyPrerequisites, []);
}
