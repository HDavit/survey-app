import {
    Request,
    ResponseObject,
    ResponseToolkit,
    Server,
} from '@hapi/hapi';
import HttpError, { ErrorOutput } from '../httpError';

export const name = 'ErrorHandler';

/**
 * respond handles re-formatting an HttpError and sending it
 *    as final response to the API
 *
 * @param httpError - An instance of HttpError
 * @param h - ResponseToolKit, used to respond to requests
 *
 * @return res - ResponseObject
 */
const respond = (
    {
 code, details, timestamp, message, statusCode,
}: ErrorOutput,
    h: ResponseToolkit,
): ResponseObject => h
    .response({
        code,
        details,
        timestamp,
        message,
    })
    .code(statusCode);

export async function register(server: Server): Promise<void> {
    server.ext('onPreResponse', (request: Request, h) => {
        if (!(request.response instanceof Error)) {
            return h.continue;
        }

        console.log(request.response)
        if (request.response instanceof HttpError) {
            return respond(request.response.getOutput(), h);
        }

        switch (request.response.output.statusCode) {
            default: {
                const err = HttpError.unexpectedError().getOutput();
                return respond(err, h);
            }
        }
    });
}
