import * as Hapi from '@hapi/hapi';

import HttpError, { ErrorOutput } from './httpError';
import { ResponseObject, ResponseToolkit } from '@hapi/hapi';

const respond = (
    {
        code, details, timestamp, message, statusCode,
    }: ErrorOutput,
    h: ResponseToolkit,
): ResponseObject => h
    .response({
        code,
        details,
        timestamp,
        message,
    })
    .code(statusCode);

export default (_request: Hapi.Request, _h: Hapi.ResponseToolkit): ResponseObject | symbol => {
    if (_request.response instanceof HttpError) {
        return respond(_request.response.getOutput(), _h);
    }
    return _h.continue;
};
