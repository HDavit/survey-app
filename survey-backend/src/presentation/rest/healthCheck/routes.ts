import { Request, ResponseObject, ResponseToolkit } from '@hapi/hapi';

import * as Joi from 'joi';

export default {
    method: 'GET',
    options: {
        auth: false as false, // eslint-disable-line @typescript-eslint/prefer-as-const
        description: 'Health Check',
        notes: 'Reports health of the service',
        response: {
            status: {
                200: Joi.object({
                    status: Joi.string()
                        .required()
                        .allow('OK')
                        .only(),
                }),
            },
        },
        tags: ['healthCheck'],
    },
    path: '/healthCheck',
    handler(request: Request, h: ResponseToolkit): ResponseObject {
        return h.response({ status: 'OK' });
    },
};
