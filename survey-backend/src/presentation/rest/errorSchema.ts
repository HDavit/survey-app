import * as Joi from 'joi';

const code = Joi
    .number()
    .required()
    .description('A 3-digit code which uniquely identify an error.');

const details = Joi
    .object()
    .description('Optional details concerning the error.');

const message = Joi
    .string()
    .required()
    .description('A description of the error.');

const timestamp = Joi
    .date()
    .timestamp()
    .description('Error timestamp');

export default {
    400: Joi
        .object({
            code,
            details,
            message,
            timestamp,
        }),
    401: Joi
        .object({
            code,
            details,
            message,
            timestamp,
        }),
    403: Joi
        .object({
            code,
            details,
            message,
            timestamp,
        }),
    404: Joi
        .object({
            code,
            details,
            message,
            timestamp,
        }),
    500: Joi
        .object({
            code,
            details,
            message,
            timestamp,
        }),
};
