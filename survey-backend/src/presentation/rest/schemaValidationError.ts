import * as Hapi from '@hapi/hapi';

import { ValidationError, ValidationErrorItem } from 'joi';
import HttpError from './httpError';

enum JoiErrorTypes {
    MissingParameter = 'any.required',
    UnExpectedParameter = 'any.unknown',
    UnExpectedObjectParameter = 'object.allowUnknown',
}

export default (
    _request: Hapi.Request,
    _h: Hapi.ResponseToolkit,
    joiError: ValidationError,
): never => {
    const stringifyPath = (path: (string | number)[]): string => path.join('.');

    const missingParameters = joiError.details.reduce(
        (acc: string[], error: ValidationErrorItem) => {
            if (error.type === JoiErrorTypes.MissingParameter) {
                acc.push(stringifyPath(error.path));
            }

            return acc;
        },
        [],
    );

    if (missingParameters.length > 0) {
        throw HttpError.missingParameters(missingParameters);
    }

    const unexpectedParameters = joiError.details.reduce(
        (acc: string[], error) => {
            if (
                error.type === JoiErrorTypes.UnExpectedParameter
                || error.type === JoiErrorTypes.UnExpectedObjectParameter
            ) {
                acc.push(stringifyPath(error.path));
            }

            return acc;
        },
        [],
    );

    if (unexpectedParameters.length > 0) {
        throw HttpError.unexpectedParameters(
            _request.server.settings.debug ? unexpectedParameters : undefined,
        );
    }

    const malformedParameters = joiError.details.reduce((acc, error) => {
        const path = stringifyPath(error.path);
        if (!acc[path]) {
            acc[path] = [];
        }

        acc[path].push(error.message);

        return acc;
    }, {});

    throw HttpError.malformedParameters(
        _request.server.settings.debug ? malformedParameters : undefined,
    );
};
