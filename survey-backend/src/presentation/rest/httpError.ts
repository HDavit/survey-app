import ErrorCode from 'core/enums/errorCode';
import HttpStatusCode from 'core/enums/httpStatusCode';
import * as dedent from 'dedent';

export interface ErrorOutput {
    code: ErrorCode;
    details: Record<string, unknown>;
    message: string;
    statusCode: HttpStatusCode;
    timestamp: number;
}

export default class HttpError extends Error {
    private readonly _code: ErrorCode;
    private readonly _details: Record<string, unknown>;
    private readonly _statusCode: HttpStatusCode;
    private readonly _timestamp: number;

    private static readonly unexpectedErrorMessage: string = dedent`
        Unexpected error: if the error persists, please contact an administrator, quoting the code and timestamp of this error`;

    private static readonly notFoundErrorMessage: string = dedent`
        The requested resource, or one of its sub-resources,
        can't be found. If the submitted query is valid, this error is likely to be caused by a
        problem with a nested resource that has been deleted or modified. Check the details
        property for additional insights.`;

    private static readonly malformedParametersMessage: string = dedent`
        At least one parameter is invalid. Examine the details
        property for more information. Invalid parameters are listed and prefixed accordingly: body for parameters
        submitted in the requests body, query for parameters appended to the requests URL, and params for
        templated parameters of the requests URL.`;

    private static readonly invalidResourceMessage: string =
        'The submitted or requested resource is invalid. If you submitted a resource, please '
        + 'check for invalid fields, which will be listed in the details property. If you '
        + 'requested a resource, ensure the latter is valid, as well as its sub-resources. ';

    private static readonly insufficientPermissionsMessage: string = dedent`
        Insufficient permissions. Your current user roles don\'t allow you to perform this
        query. Should you believe this error to be incorrect, please contact an administrator.`;

    private static readonly unauthenticatedRequestMessage: string = dedent`
        Access is restricted to authenticated users only. The query can't be made without a valid API key (check the X-APIKEY header of your request).`;

    private static readonly missingParametersMessage: string = dedent`
        At least one parameter is missing from the request body.
        Ensure you have provided all required parameters, as specified in the documentation.`;

    private static readonly unexpectedParametersMessage: string = dedent`
        At least one parameter is unexpected, or unwanted. If
        you are trying to update a resource, be sure to include only parameters that you want to, and may, update.
        Some parameters are read-only or create-only.`;

    private static readonly conflictingParametersMessage: string =
        'Parameters in the request body are conflicting with one another. It\'s likely that they are valid '
        + 'individually, yet they can\'t be submitted together, as that would lead to invalid '
        + 'combinations.';

    constructor(
        httpStatusCode: HttpStatusCode,
        errorCode: ErrorCode,
        message: string,
        details: Record<string, unknown> = {},
    ) {
        super(message);
        this._code = errorCode;
        this._details = details;
        this._statusCode = httpStatusCode;
        this._timestamp = new Date().getTime();
    }

    /**
     * getOutput returns the error data
     *
     * @return {ErrorOutput} output
     */
    public getOutput(): ErrorOutput {
        return {
            code: this._code,
            details: this._details,
            message: this.message,
            statusCode: this._statusCode,
            timestamp: this._timestamp,
        };
    }

    public static unexpectedError(details?: Record<string, unknown>): HttpError {
        return new HttpError(
            HttpStatusCode.InternalServerError,
            ErrorCode.UnExpectedError,
            this.unexpectedErrorMessage,
            details,
        );
    }

    /**
     *
     * @param parameters
     *
     * SCHEMA: {
     *      [parameter1]: string[],
     *      [parameter2]: string[],
     *      ...
     * }
     *
     * EXAMPLE: {
     *      amount: [
     *          exceeds maximum character length of 16,
     *          contains invalid character: %
     *      ],
     *      account.currencyCode: [
     *          is not one of allowed values: EUR, USD, GBP
     *      ]
     * }
     */
    public static malformedParameters(parameters?: Record<string, unknown>): HttpError {
        const details = parameters
            ? { malformedParameters: parameters }
            : undefined;

        return new HttpError(
            HttpStatusCode.BadRequest,
            ErrorCode.MalformedParamaters,
            this.malformedParametersMessage,
            details,
        );
    }

    public static notFoundError(details?: Record<string, unknown>): HttpError {
        return new HttpError(
            HttpStatusCode.NotFound,
            ErrorCode.NotFound,
            this.notFoundErrorMessage,
            details,
        );
    }

    public static invalidResourceError(details?: Record<string, unknown>): HttpError {
        return new HttpError(
            HttpStatusCode.BadRequest,
            ErrorCode.InvalidResource,
            this.invalidResourceMessage,
            details,
        );
    }

    public static insufficientPermissionsError(details?: Record<string, unknown>): HttpError {
        return new HttpError(
            HttpStatusCode.Forbidden,
            ErrorCode.InsufficientPermissions,
            this.insufficientPermissionsMessage,
            details,
        );
    }

    public static unauthenticatedRequest(details?: Record<string, unknown>): HttpError {
        return new HttpError(
            HttpStatusCode.Unauthorized,
            ErrorCode.UnAuthenticatedRequest,
            this.unauthenticatedRequestMessage,
            details,
        );
    }

    public static missingParameters(parameters: string[]): HttpError {
        return new HttpError(
            HttpStatusCode.BadRequest,
            ErrorCode.MissingParameters,
            this.missingParametersMessage,
            {
                missingParameters: parameters,
            },
        );
    }

    public static unexpectedParameters(parameters?: string[]): HttpError {
        const details = parameters
            ? { unexpectedParameters: parameters }
            : undefined;

        return new HttpError(
            HttpStatusCode.BadRequest,
            ErrorCode.UnExpectedParameters,
            this.unexpectedParametersMessage,
            details,
        );
    }

    public static conflictingParameters(
        parameters: string[],
        reason: string,
    ): HttpError {
        return new HttpError(
            HttpStatusCode.BadRequest,
            ErrorCode.ConflictingParameters,
            this.conflictingParametersMessage,
            { parameters, reason },
        );
    }
}
