import container from 'container';

const resolve = <T>(dependency: symbol) => (): T => container.get<T>(dependency);

const lazyHapiResolve = <T>(
    dependency: symbol,
): { assign: string; method: () => T } => {
    return {
        assign: dependency.toString(),
        method: resolve<T>(dependency),
    };
};

export default lazyHapiResolve;
