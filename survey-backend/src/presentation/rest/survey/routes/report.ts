import {
    ResponseToolkit, ServerRoute, ResponseObject,
} from '@hapi/hapi';
import { constants } from 'http2';

import TYPES from 'core/types';
import lazyResolve from 'presentation/rest/lazyHapiResolve';
import schema from 'presentation/rest/survey/schema/generateReport';
import HttpError from 'presentation/rest/httpError';
import { Request } from '@hapi/hapi';
import ResultType from 'core/enums/resultType';
import GetReport from 'app/survey/getReport';

const route: ServerRoute = {
    method: 'GET',
    options: {
        description: 'Generate Survey Report',
        tags: ['api', 'survey'],
        pre: [
            lazyResolve<GetReport>(TYPES.GetReport),
        ],
        validate: {
            payload: schema.payload,
        },
        response: {
            status: {
                [constants.HTTP_STATUS_OK]: schema.response,
            },
        },
    },
    path: '/report',
    async handler(request: Request, h: ResponseToolkit): Promise<ResponseObject | HttpError> {
        const getReport: GetReport = request.pre[TYPES.GetReport.toString()];

        const result = await getReport.execute();

        switch (result.resultType) {
            case ResultType.Success: {
                return h
                    .response(result.value)
                    .code(constants.HTTP_STATUS_OK);
            }
            default: {
                return HttpError.unexpectedError();
            }
        }
    },
};

export default route;
