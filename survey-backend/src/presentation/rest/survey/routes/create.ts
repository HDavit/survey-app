import {
    ResponseToolkit, ServerRoute, ResponseObject,
} from '@hapi/hapi';
import { constants } from 'http2';

import TYPES from 'core/types';
import lazyResolve from 'presentation/rest/lazyHapiResolve';
import schema from 'presentation/rest/survey/schema/create';
import HttpError from 'presentation/rest/httpError';
import { Request } from '@hapi/hapi';
import ResultType from 'core/enums/resultType';
import CreateSurvey from 'app/survey/create';
import CreateSurveyPayload from 'core/interfaces/survey/create';

const route: ServerRoute = {
    method: 'POST',
    options: {
        description: 'Create Survey',
        tags: ['api', 'survey'],
        pre: [
            lazyResolve<CreateSurvey>(TYPES.CreateSurvey),
        ],
        validate: {
            payload: schema.payload,
        },
        response: {
            status: {
                [constants.HTTP_STATUS_CREATED]: schema.response,
            },
        },
    },
    path: '/survey',
    async handler(request: Request, h: ResponseToolkit): Promise<ResponseObject | HttpError> {
        const payload = request.payload as CreateSurveyPayload;
        const createSurvey: CreateSurvey = request.pre[TYPES.CreateSurvey.toString()];

        const result = await createSurvey.execute(payload);

        switch (result.resultType) {
            case ResultType.Success: {
                return h
                    .response(result.value)
                    .code(constants.HTTP_STATUS_OK);
            }
            default: {
                return HttpError.unexpectedError();
            }
        }
    },
};

export default route;
