import create from './create';
import report from './report';

export default [
    create,
    report,
];
