import * as joi from 'joi';
import {countryListAlpha3, genders} from 'presentation/rest/survey/schema/constants';

const schema = {
    payload: undefined,
    response: joi.object({
        count: joi.number().description('The count of surveys that already have been taken'),
        averageAge: joi.number().description('The average age of survey takers'),
        averageExperience: joi.number().description('The average experience of survey takers'),
        countryDistribution: joi.array().items(joi.object().pattern(joi.string().valid(...countryListAlpha3), joi.number())).description('Distributions by country'),
        genderDistribution: joi.array().items(joi.object().pattern(joi.string().valid(...genders), joi.number())).description('Distributions by gender'),
    }),
};

export default schema;
