import * as joi from 'joi';
import { countryListAlpha3, genders } from 'presentation/rest/survey/schema/constants';

const schema = {
    payload: joi.object({
        name: joi.string().required().max(36).description('Survey taker name'),
        email: joi.string().required().description('Survey taker email address'),
        age: joi.number().description('Survey taker age'),
        gender: joi.string().valid(...genders).description('Survey taker gender'),
        country: joi.string().valid(...countryListAlpha3).description('Survey taker country'),
        experience: joi.number().min(1).max(5).description('Experience rating'),
        suggestion: joi.string().description('Suggested improvements'),
        originPage: joi.string().description('hidden and pre-filled with the page the user came from'),
    }),
    response: joi.object({
        id: joi.string().description('The id of survey'),
        name: joi.string().required().max(36).description('Survey taker name'),
        email: joi.string().required().description('Survey taker email address'),
        age: joi.number().description('Survey taker age'),
        gender: joi.string().valid(...genders).description('Survey taker gender'),
        country: joi.string().valid(...countryListAlpha3).description('Survey taker country'),
        experience: joi.number().min(1).max(5).description('Experience rating'),
        suggestion: joi.string().description('Suggested improvements'),
        originPage: joi.string().description('hidden and pre-filled with the page the user came from'),
    }),
};

export default schema;
