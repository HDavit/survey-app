import { Request, ResponseObject, ResponseToolkit } from '@hapi/hapi';

export default {
    method: 'GET',
    path: '/docs',
    options: {
        auth: false as false, // eslint-disable-line @typescript-eslint/prefer-as-const
    },
    handler(request: Request, h: ResponseToolkit): ResponseObject {
        return h.file('dist/index.html', { confine: true });
    },
};
