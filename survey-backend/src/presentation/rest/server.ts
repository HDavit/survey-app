import * as Hapi from '@hapi/hapi';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import * as Blipp from 'blipp';
import * as HapiSwagger from 'hapi-swagger';
import { inject, injectable } from 'inversify';

import Config from 'config';

import TYPES from 'core/types';
import Env from 'core/enums/env';

import { Log } from 'core/interfaces/log';
import PresentationLayer from 'core/interfaces/presentationLayer';

import healthCheckRoutes from 'presentation/rest/healthCheck/routes';
import APIRoute from 'presentation/rest/apiDoc/routes';
import surveyRouter from 'presentation/rest/survey/routes';

import InversifyHapi from 'presentation/rest/plugins/inversifyHapi';
import SchemaValidationError from 'presentation/rest/schemaValidationError';
import ResponseValidationError from 'presentation/rest/responseValidationError';
import * as ErrorHandler from 'presentation/rest/plugins/errorHandler';

@injectable()
export default class Server implements PresentationLayer {
    private readonly _server: Hapi.Server;
    private readonly _config: Config;

    constructor(
        @inject(TYPES.Config) _config: Config,
        @inject(TYPES.InversifyHapi) private readonly inversifyHapi: InversifyHapi,
        @inject(TYPES.Log) log: Log,
    ) {
        this._config = _config;
        const { host, port } = this._config.http;
        const { env } = this._config;
        log.info(`Starting HTTP server on http://${host}:${port}`);

        this._server = new Hapi.Server({
            host,
            port,
            routes: {
                validate: {
                    failAction: SchemaValidationError,
                    options: {
                        abortEarly: false,
                    },
                },
                response: {
                    failAction: ResponseValidationError,
                },
                cors: {
                    origin: ['*'],
                    headers: ['Authorization', 'Content-Type'],
                    maxAge: 60,
                    credentials: true,
                },
            },
            router: {
                isCaseSensitive: true,
                stripTrailingSlash: true,
            },
            ...(env === Env.Development && {
                debug: {
                    log: ['*'],
                    request: ['*'],
                },
            }),
        });
    }

    public async mount(): Promise<void> {
        this._server.route(healthCheckRoutes);
        this._server.route(APIRoute);
        this._server.route(surveyRouter);
    }

    public async start(): Promise<void> {
        await this.register();
        await this.mount();
        return this._server.start();
    }

    // don't remove hapi-swagger, we're using it to generate swagger.json
    public async register(): Promise<void> {
        await this._server.register([
            this.inversifyHapi,
            Blipp,
            Vision,
            Inert,
            ErrorHandler,
            {
                options: {
                    auth: false,
                    basePath: '/',
                    info: {
                        title: 'Survey Service',
                        description: 'Survey Service APIs',
                    },
                    grouping: 'tags',
                    swaggerUI: false,
                    documentationPage: false,
                    sortEndpoints: 'method',
                    produces: ['application/json'],
                },
                plugin: HapiSwagger,
            },
        ] as Array<Hapi.ServerRegisterPluginObject<unknown>>);
    };

    get server(): Hapi.Server {
        return this._server;
    }
}
