interface Operation<R, T, E> {
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    execute(...args): Promise<{ resultType: R; value?: T; appError?: E }>;
}

export default Operation;
