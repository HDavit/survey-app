import { inject, injectable } from 'inversify';
import 'reflect-metadata';

import TYPES from 'core/types';

import { Log, LogFactory } from 'core/interfaces/log';

import Timer from 'core/interfaces/timer';
import Instrumentation from 'core/interfaces/instrumentation';
import Traceable from 'core/interfaces/traceable';

@injectable()
export default class CreateSurveyInstrumentation implements Instrumentation, Traceable {
    private readonly log: Log;

    constructor(@inject(TYPES.LogFactory) logFactory: LogFactory, @inject(TYPES.Timer) private readonly timer: Timer) {
        this.log = logFactory('CreateSurvey');
    }

    public setMetadata(metadata: Record<string, unknown>): this {
        this.log.setMetadata(metadata);
        return this;
    }

    public setTraceId(traceId: string): this {
        this.log.setTraceId(traceId);
        return this;
    }

    public success(): void {
        this.log.info('Survey created successfully', {
            elapsedTime: this.timer.elapsed(),
        });
        this.timer.reset();
    }

    public unexpectedError(err: Error): void {
        this.log.error('Unexpected error while creating survey', err, {
            elapsedTime: this.timer.elapsed(),
        });
    }
}
