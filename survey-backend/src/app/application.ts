import { inject, injectable } from 'inversify';

import TYPES from 'core/types';

import PresentationLayer from 'core/interfaces/presentationLayer';
import DatabaseClient from 'infra/modules/dbClient';

@injectable()
export default class Application {
    constructor(
        @inject(TYPES.PresentationLayer) private readonly presentationLayer: PresentationLayer,
        @inject(TYPES.DatabaseClient) private readonly databaseClient: DatabaseClient,
    ) {
    }

    public async start(): Promise<void> {
        await this.presentationLayer.start();
        await this.databaseClient.init();
    }
}
