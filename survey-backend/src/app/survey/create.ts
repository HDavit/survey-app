import { inject, injectable } from 'inversify';
import ApplicationError from 'core/interfaces/applicationError';
import ResultType from 'core/enums/resultType';
import Operation from 'app/operation';

import TYPES from 'core/types';
import CreateSurveyInstrumentation from 'app/instrumentation/survey/create';
import CreateSurveyPayload, {CreateSurveyResponse} from 'core/interfaces/survey/create';
import InsertSurvey from 'infra/repositories/survey/insert';

export interface CreateSurveyResult {
    resultType: ResultType;
    value?: CreateSurveyResponse
    appError?: ApplicationError;
}

@injectable()
export default class CreateSurvey implements Operation<ResultType, CreateSurveyPayload, ApplicationError> {
    constructor(
        @inject(TYPES.CreateSurveyInstrumentation) private readonly instrumentation: CreateSurveyInstrumentation,
        @inject(TYPES.InsertSurvey) private readonly insertSurvey: InsertSurvey,
    ) {
    }

    public async execute(
        payload: CreateSurveyPayload,
    ): Promise<CreateSurveyResult> {
        try {
            this.instrumentation.setMetadata({ ...payload });

            const survey = await this.insertSurvey.execute(payload);
            this.instrumentation.success();

            return {
                resultType: ResultType.Success,
                value: survey,
            };
        } catch (e) {
            this.instrumentation.unexpectedError(e);
            throw Error(e);
        }
    }
}
