import { inject, injectable } from 'inversify';
import ApplicationError from 'core/interfaces/applicationError';
import ResultType from 'core/enums/resultType';
import Operation from 'app/operation';

import TYPES from 'core/types';
import GetReportInstrumentation from 'app/instrumentation/survey/getReport';
import GenerateReport from 'infra/repositories/survey/report';
import Report from 'core/interfaces/survey/report';

export interface GenerateReportResult {
    resultType: ResultType;
    value?: Report
    appError?: ApplicationError;
}

@injectable()
export default class GetReport implements Operation<ResultType, Report, ApplicationError> {
    constructor(
        @inject(TYPES.GetReportInstrumentation) private readonly instrumentation: GetReportInstrumentation,
        @inject(TYPES.GenerateReport) private readonly generateReport: GenerateReport,
    ) {
    }

    public async execute(): Promise<GenerateReportResult> {
        try {
            this.instrumentation.success();

            const res = await this.generateReport.execute();

            return {
                resultType: ResultType.Success,
                value: res,
            };
        } catch (e) {
            this.instrumentation.unexpectedError(e);
            throw Error(e);
        }
    }
}
