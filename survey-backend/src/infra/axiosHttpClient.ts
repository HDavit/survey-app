import { inject, injectable } from 'inversify';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import * as https from 'https';
import * as http from 'http';
import TYPES from 'core/types';

import {
    Headers,
    HttpClient,
    HttpClientResponse,
    Transformer,
} from 'core/interfaces/clients/http';
import { Log } from 'core/interfaces/log';

type ExecuteRequest = (...args: unknown[]) => Promise<AxiosResponse<Record<string, unknown>>>;

@injectable()
export default class AxiosHttpClient implements HttpClient {
    private _body: string;
    private _query: Record<string, string | number> = {};
    private _headers: Headers;
    private _baseHeaders: Headers;
    private _rootCa: Buffer | string;
    private _baseUrl: string;
    private _timeout: number;
    private _transformer: Transformer;
    private _strictSSL = true;

    constructor(@inject(TYPES.Log) private readonly log: Log) {
    }

    public body(body: Record<string, unknown>): this {
        if (typeof body === 'string') {
            this._body = body;
        } else {
            this._body = JSON.stringify(body);
        }

        return this;
    }

    public baseHeaders(headers: Headers): this {
        this._baseHeaders = {
            ...this._baseHeaders,
            ...headers,
        };

        return this;
    }

    public query(query: Record<string, string | number>): this {
        this._query = {
            ...this._query,
            ...query,
        };

        return this;
    }

    public headers(headers: Headers): this {
        this._headers = {
            ...this._headers,
            ...headers,
        };

        return this;
    }

    public strictSSL(strictSSL: boolean): this {
        this._strictSSL = strictSSL;
        return this;
    }

    public rootCa(rootCa: Buffer | string): this {
        this._rootCa = rootCa;
        return this;
    }

    public baseUrl(url: string): this {
        this._baseUrl = url;
        return this;
    }

    public timeout(time: number): this {
        this._timeout = time;
        return this;
    }

    public transform(transformer: Transformer): this {
        this._transformer = transformer;
        return this;
    }

    private static async unwrapAsyncResponse(
        request: ExecuteRequest,
    ): Promise<AxiosResponse> {
        try {
            return await request();
        } catch (e) {
            const { response } = e as AxiosError;
            if (!response) {
                throw e;
            }
            return response;
        }
    }

    public async get<ResponseBody>(
        url: string = '/',
        options?: AxiosRequestConfig,
    ): Promise<HttpClientResponse<ResponseBody>> {
        this.log.debug('Request HTTP client GET request', { url });

        const {
            status,
            headers,
            data,
        }: AxiosResponse = await AxiosHttpClient.unwrapAsyncResponse(async () => axios.get(url, {
            ...this.constructOptions(url),
            ...options,
        }),
        );

        return this.constructResponse(status, headers, data);
    }

    public async post<ResponseBody>(
        url: string = '/',
    ): Promise<HttpClientResponse<ResponseBody>> {
        this.log.debug('Request HTTP client POST request', { url });

        const {
            status,
            headers,
            data,
        }: AxiosResponse = await AxiosHttpClient.unwrapAsyncResponse(
            async () => axios.post(url, this._body, this.constructOptions(url, this._body)),
        );

        return this.constructResponse(status, headers, data);
    }

    public async put<ResponseBody>(
        url: string = '/',
    ): Promise<HttpClientResponse<ResponseBody>> {
        this.log.debug('Request HTTP client PUT request', { url });

        const {
            status,
            headers,
            data,
        }: AxiosResponse = await AxiosHttpClient.unwrapAsyncResponse(
            async () => axios.put(url, this._body, this.constructOptions(url, this._body)),
        );

        return this.constructResponse(status, headers, data);
    }

    public async delete<ResponseBody>(
        url: string = '/',
    ): Promise<HttpClientResponse<ResponseBody>> {
        this.log.debug('Request HTTP client DELETE request', { url });

        const {
            status,
            headers,
            data,
        }: AxiosResponse = await AxiosHttpClient.unwrapAsyncResponse(async () => axios
            .delete(url, this.constructOptions(url)),
        );

        return this.constructResponse(status, headers, data);
    }

    public async patch<ResponseBody>(
        url: string = '/',
    ): Promise<HttpClientResponse<ResponseBody>> {
        this.throwIfBodyMissing();

        this.log.debug('Request HTTP client PATCH request', { url });

        const {
            status,
            headers,
            data,
        }: AxiosResponse = await AxiosHttpClient.unwrapAsyncResponse(async () => axios.patch(
            url,
            this._body,
            this.constructOptions(url, this._body),
        ),
        );

        return this.constructResponse(status, headers, data);
    }

    private constructOptions(
        url: string,
        requestBody?: string,
    ): AxiosRequestConfig {
        const caOptions = this._rootCa ? { ca: this._rootCa } : {};

        const commonHttpConfig = { keepAlive: true };

        const httpsAgent = new https.Agent({
            rejectUnauthorized: this._strictSSL,
            ...caOptions,
            // keep sockets alive between requests
            ...commonHttpConfig,
        });

        const httpAgent = new http.Agent(commonHttpConfig);

        const options: AxiosRequestConfig = {
            headers: this._headers,
            params: this._query,
            url,
            httpsAgent,
            httpAgent,
        };

        if (this._baseUrl) {
            options.baseURL = this._baseUrl;
        }

        if (this._timeout) {
            options.timeout = this._timeout;
        }

        if (requestBody) {
            options.data = requestBody;
        }

        return options;
    }

    private constructResponse<ResponseBody>(
        statusCode: number,
        headers: Record<string, unknown>,
        body: string,
    ): HttpClientResponse<ResponseBody> {
        const isString = typeof body === 'string';
        let responseBody: unknown = body;

        if (this._transformer && isString && body.length > 0) {
            responseBody = this._transformer(body);
        }

        return {
            body: responseBody as ResponseBody,
            headers,
            statusCode,
        };
    }

    private throwIfBodyMissing(): void {
        if (!this._body) {
            throw new Error('Request missing body');
        }
    }
}
