import { inject, injectable } from 'inversify';
import TYPES from 'core/types';
import DatabaseClient from 'infra/modules/dbClient';
import DatabaseOperation from 'infra/repositories/databaseOperation';
import { Survey } from 'infra/models/survey';
import Report from 'core/interfaces/survey/report';
import * as Sequelize from 'sequelize';

interface Distribution {
    countryDistribution: {
        country: string,
        countryCount: number,
    }[],
    genderDistribution: {
        gender: string,
        genderCount: number,
    }[],
}

@injectable()
export default class GenerateReport implements DatabaseOperation<any> {
    constructor(@inject(TYPES.DatabaseClient) private readonly databaseClient: DatabaseClient) {

    }

    public async execute(): Promise<Report> {
        const { sequelize } = this.databaseClient;

        const [
            averagesResult,
            countryDistribution,
            genderDistribution,
        ] = await Promise.all([
            Survey.findAll({
                attributes: [
                    [sequelize.fn('AVG', sequelize.col('age')), 'averageAge'],
                    [sequelize.fn('AVG', sequelize.col('experience')), 'averageExperience'],
                    [sequelize.fn('COUNT', sequelize.col('id')), 'numberOfRespondents'],
                ],
                raw: true,
            }),
            Survey.findAll({
                group: ['country'],
                attributes: ['country', [sequelize.fn('COUNT', 'country'), 'countryCount']],
                where: {
                    country: {
                        [Sequelize.Op.ne]: null
                    }
                },
                raw: true,
            }),
            Survey.findAll({
                group: ['gender'],
                attributes: ['gender', [sequelize.fn('COUNT', 'gender'), 'genderCount']],
                where: {
                    gender: {
                        [Sequelize.Op.ne]: null
                    }
                },
                raw: true,
            }),
        ]);

        const averages = averagesResult[0] as unknown as {
            numberOfRespondents: string,
            averageAge: number,
            averageExperience: number,
        };

        return {
            count: Number(averages.numberOfRespondents),
            averageAge: this.roundNumber(averages.averageAge),
            averageExperience: this.roundNumber(averages.averageExperience),
            ...this.adapt({
                countryDistribution,
                genderDistribution,
            } as unknown as Distribution),
        };
    }

    roundNumber(num: number): number {
        return Math.round(num * 100) / 100;
    }

    adapt(
        distributions: Distribution,
    ) {
        return {
            countryDistribution: distributions.countryDistribution.map(distribution => ({
                [distribution.country]: Number(distribution.countryCount),
            })),
            genderDistribution: distributions.genderDistribution.map(distribution => ({
                [distribution.gender]: Number(distribution.genderCount),
            })),
        };
    }
}
