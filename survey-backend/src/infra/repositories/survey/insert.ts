import { inject, injectable } from 'inversify';
import TYPES from 'core/types';
import DatabaseClient from 'infra/modules/dbClient';
import DatabaseOperation from 'infra/repositories/databaseOperation';
import { Survey } from 'infra/models/survey';
import CreateSurveyPayload, { CreateSurveyResponse } from 'core/interfaces/survey/create';

@injectable()
export default class InsertSurvey implements DatabaseOperation<CreateSurveyResponse> {
    constructor(@inject(TYPES.DatabaseClient) private readonly databaseClient: DatabaseClient) {
    }

    public async execute(payload: CreateSurveyPayload): Promise<CreateSurveyResponse> {
        const survey = await Survey.create(payload as any, {
            raw: true,
        });

        return {
            id: survey.getDataValue('id'),
            ...payload,
        };
    }
}
