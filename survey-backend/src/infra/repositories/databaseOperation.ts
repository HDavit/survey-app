interface DatabaseOperation<R> {
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    execute(...args): Promise<R>;
}

export default DatabaseOperation;
