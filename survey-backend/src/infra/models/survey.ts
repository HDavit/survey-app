import { Table, Column, Model } from 'sequelize-typescript';
import {countryListAlpha3} from "presentation/rest/survey/schema/constants";

@Table
export class Survey extends Model<Survey> {
    @Column
    name!: string;

    @Column({ unique: true })
    email!: string;

    @Column
    age: number;

    @Column({ type: 'text', values: ['male', 'female'] })
    gender: string;

    @Column({ type: 'text', values: countryListAlpha3 })
    country: string;

    @Column
    experience: number;

    @Column
    suggestion: string;

    @Column
    originPage: string;
}
