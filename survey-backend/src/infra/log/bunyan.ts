import * as bunyan from 'bunyan';
import { inject, injectable } from 'inversify';

import Config from 'config';
import TYPES from 'core/types';

import LogLevel, { LogType } from 'core/enums/logLevel';
import { Log } from 'core/interfaces/log';

@injectable()
export default class BunyanLog implements Log {
    private readonly logName: string;
    private readonly logLevel: LogLevel;
    private readonly bunyanLog: bunyan;
    private _metadata: Record<string, unknown> = {};

    constructor(@inject(TYPES.Config) config: Config) {
        this.logName = config.name;
        this.logLevel = config.log.level;
        const options: bunyan.LoggerOptions = {
            name: config.name,
            level: config.log.level,
        };
        switch (config.log.type) {
            case LogType.File:
                options.streams = [
                    {
                        path: config.log.path,
                    },
                ];
                break;
            case LogType.RotatingFile:
                options.streams = [
                    {
                        path: config.log.path,
                        period: config.log.period,
                        count: config.log.count,
                    },
                ];
                break;
            case LogType.Stream:
            default:
                options.streams = [
                    {
                        stream: process.stdout,
                    },
                ];
                break;
        }
        this.bunyanLog = bunyan.createLogger(options);
    }

    public setMetadata(metadata: Record<string, unknown>): this {
        this._metadata = {
            ...this._metadata,
            ...metadata,
        };

        return this;
    }

    public setContext(ctx: string): this {
        this._metadata = {
            ...this._metadata,
            ctx,
        };

        return this;
    }

    public setTraceId(traceId: string): this {
        this.setMetadata({ traceId });
        return this;
    }

    public error(message: string, error: Error, metadata?: Record<string, unknown>): void {
        this.bunyanLog.error(
            {
                error: error.stack,
                ...(metadata || {}),
            },
            message,
        );
    }

    public warn(message: string, metadata?: Record<string, unknown>): void {
        this.bunyanLog.warn(
            {
                ...this._metadata,
                ...metadata,
            },
            message,
        );
    }

    public info(message: string, metadata?: Record<string, unknown>): void {
        this.bunyanLog.info(
            {
                ...this._metadata,
                ...metadata,
            },
            message,
        );
    }

    public debug(message: string, metadata?: Record<string, unknown>): void {
        this.bunyanLog.debug(
            {
                ...this._metadata,
                ...metadata,
            },
            message || {},
        );
    }

    public trace(message: string, metadata?: Record<string, unknown>): void {
        this.bunyanLog.trace(
            {
                ...this._metadata,
                ...metadata,
            },
            message,
        );
    }
}
