/* eslint-disable @typescript-eslint/no-unused-vars */
import { injectable } from 'inversify';

import { Log } from 'core/interfaces/log';

@injectable()
export default class StubLog implements Log {
    public setMetadata(metadata: Record<string, unknown>): this {
        return this;
    }

    public setContext(context: string): this {
        return this;
    }

    public setTraceId(traceId: string): this {
        return this;
    }

    public error(message: string, error: Error, metadata?: Record<string, unknown>): void {
        return null;
    }

    public warn(message: string, metadata?: Record<string, unknown>): void {
        return null;
    }

    public info(message: string, metadata?: Record<string, unknown>): void {
        return null;
    }

    public debug(message: string, metadata?: Record<string, unknown>): void {
        return null;
    }

    public trace(message: string, metadata?: Record<string, unknown>): void {
        return null;
    }
}
