import { injectable } from 'inversify';

import Timer from 'core/interfaces/timer';

@injectable()
export default class RequestTimer implements Timer {
    private start: number = new Date().getTime()

    public elapsed(): number {
        return new Date().getTime() - this.start;
    }

    public reset(): void {
        this.start = new Date().getTime();
    }
}
