import { Sequelize } from 'sequelize-typescript';
import { Survey } from 'src/infra/models/survey';
import { Pool } from 'pg';
import Config from 'config';
import { inject, injectable } from 'inversify';
import TYPES from 'core/types';
import { Log, LogFactory } from 'core/interfaces/log';

@injectable()
export default class DatabaseClient {
    public sequelize: any;
    private readonly log: Log;

    constructor(
        @inject(TYPES.LogFactory) logFactory: LogFactory,
        @inject(TYPES.Config) private readonly config: Config,
    ) {
        this.log = logFactory('CreateSurvey');
    }

    async init(): Promise<DatabaseClient> {
        this.sequelize = new Sequelize({
            dialect: 'postgres',
            models: [Survey],
            username: this.config.db.user,
            password: this.config.db.password,
            host: this.config.db.host,
            database: this.config.db.name,
        });

        await this.syncDB();

        return this;
    }

    async createDBIfNotExists(): Promise<void> {
        try {
            const pg = new Pool({
                user: this.config.db.user,
                password: this.config.db.password,
                host: this.config.db.host,
            });
            await pg.query(`CREATE DATABASE ${this.config.db.name}`);
        } catch (e) {}
    }

    async syncDB(): Promise<void> {
        try {
            await this.createDBIfNotExists();
            await this.sequelize.sync();
            this.log.info('db synced');
        } catch (err) {
            this.log.error('Cannot connect to DB', err);
            throw err;
        }
    }
}
