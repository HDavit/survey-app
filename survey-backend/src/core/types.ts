export default {
    Application: Symbol.for('Application'),
    Config: Symbol.for('config'),
    InversifyHapi: Symbol.for('InversifyHapi'),
    Log: Symbol.for('Log'),
    LogFactory: Symbol.for('LogFactory'),
    Timer: Symbol.for('Timer'),
    PresentationLayer: Symbol.for('PresentationLayer'),

    CreateSurvey: Symbol.for('CreateSurvey'),
    CreateSurveyInstrumentation: Symbol.for('CreateSurveyInstrumentation'),

    GetReport: Symbol.for('GetReport'),
    GetReportInstrumentation: Symbol.for('GetReportInstrumentation'),

    HttpClient: Symbol.for('HttpClient'),

    DatabaseClient: Symbol.for('DatabaseClient'),
    InsertSurvey: Symbol.for('InsertSurvey'),
    GenerateReport: Symbol.for('GenerateReport'),
};
