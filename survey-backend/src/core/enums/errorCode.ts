enum ErrorCode {
    UnExpectedError = 100,
    MalformedParamaters = 107,
    NotFound = 109,
    InvalidResource = 110,
    InsufficientPermissions = 113,
    UnAuthenticatedRequest = 116,
    MissingParameters = 126,
    UnExpectedParameters = 127,
    ConflictingParameters = 137,
}

export default ErrorCode;
