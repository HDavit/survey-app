enum Env {
    Production = 'production',
    Testing = 'testing',
    Development = 'development',
    Staging = 'staging',
    Sandbox = 'sandbox',
}

export default Env;
