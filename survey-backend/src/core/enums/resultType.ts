enum ResultType {
    Success = 'SUCCESS',
    UnexpectedError = 'UNEXPECTED_ERROR',
}

export default ResultType;
