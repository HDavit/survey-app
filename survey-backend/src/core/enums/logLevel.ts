enum LogLevel {
    Error = 'error',
    Warn = 'warn',
    Info = 'info',
    Debug = 'debug',
    Trace = 'trace',
}

export enum LogType {
    Stream = 'stream',
    File = 'file',
    RotatingFile = 'rotating-file',
}

export default LogLevel;
