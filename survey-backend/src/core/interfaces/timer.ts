interface Timer {
    elapsed(): number

    reset(): void
}

export default Timer;
