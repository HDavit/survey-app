interface Traceable {
    setTraceId(traceId: string): this;
}

export default Traceable;
