interface Instrumentation {
    setMetadata(metadata: Record<string, unknown>): this;
};

export default Instrumentation;
