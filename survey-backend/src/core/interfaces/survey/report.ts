interface Report {
    count: number,
    averageAge: number,
    averageExperience: number,
    countryDistribution: Record<string, number>[],
    genderDistribution: Record<string, number>[],
}

export default Report;
