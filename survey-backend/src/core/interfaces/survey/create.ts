interface CreateSurveyPayload {
    name: string,
    email: string,
    age?: string,
    gender?: string,
    country?: string,
    experience?: string,
    suggestion?: string,
    originPage?: string,
}

export interface CreateSurveyResponse extends CreateSurveyPayload{
    id: number,
}

export default CreateSurveyPayload;
