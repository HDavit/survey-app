export interface HttpClientResponse<ResponseBody> {
    statusCode: number;
    headers: Record<string, unknown>;
    body: ResponseBody;
}

export interface Headers {
    [name: string]: string;
}

export type Transformer = (response: string) => unknown;

export interface HttpClient {
    body(body: Record<string, unknown>[] | Record<string, unknown> | string): this;

    query(query: Record<string, unknown>): this;

    headers(headers: Headers): this;

    rootCa(rootCa: Buffer | string): this;

    strictSSL(strictSSL: boolean): this;

    baseUrl(url: string): this;

    baseHeaders(headers: Headers): this;

    timeout(timeout: number): this;

    transform(transformer: Transformer): this;

    get<ResponseBody = string>(uri?: string): Promise<HttpClientResponse<ResponseBody>>;

    post<ResponseBody = string>(uri?: string): Promise<HttpClientResponse<ResponseBody>>;

    put<ResponseBody = string>(uri?: string): Promise<HttpClientResponse<ResponseBody>>;

    delete<ResponseBody = string>(uri?: string): Promise<HttpClientResponse<ResponseBody>>;

    patch<ResponseBody = string>(uri?: string): Promise<HttpClientResponse<ResponseBody>>;
}
