import Instrumentation from 'core/interfaces/instrumentation';
import Traceable from 'core/interfaces/traceable';

export interface Log extends Instrumentation, Traceable {
    error: (message: string, error: Error, metadata?: Record<string, unknown>) => void;
    warn: (message: string, metadata?: Record<string, unknown>) => void;
    info: (message: string, metadata?: Record<string, unknown>) => void;
    debug: (message: string, metadata?: Record<string, unknown>) => void;
    trace: (message: string, metadata?: Record<string, unknown>) => void;
    setContext: (ctx: string) => this;
}

export type LogFactory = (ctx: string) => Log;
