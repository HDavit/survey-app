interface PresentationLayer {
    start(): Promise<void>;
};

export default PresentationLayer;
