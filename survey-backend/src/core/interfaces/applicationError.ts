interface ApplicationError {
    value: Record<string, unknown>
};

export default ApplicationError;
