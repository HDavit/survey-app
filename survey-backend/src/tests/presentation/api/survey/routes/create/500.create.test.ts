import * as chai from 'chai';
import 'mocha';

import container from 'container';
import TYPES from 'core/types';

import Server from 'presentation/rest/server';

import * as fixture from './500.create.fixture';

const { expect } = chai;

describe('presentation - rest - survey - create - 500', () => {
    beforeEach(fixture.setup);
    afterEach(fixture.restore);

    it('returns unexpected error', async () => {
        const presentationLayer: Server = container.get<Server>(
            TYPES.PresentationLayer,
        );
        await presentationLayer.register();
        await presentationLayer.mount();

        const response = await presentationLayer.server.inject({
            method: 'POST',
            url: fixture.url,
            payload: fixture.mockPayload,
        });

        expect(response.statusCode).to.equal(500);

        expect(response).to.have.property('payload');

        const payload = JSON.parse(response.payload);

        expect(payload.code).to.equal(fixture.error.code);
        expect(payload.message).to.equal(fixture.error.message);
    });
});
