import container from 'container';
import StubLog from 'infra/log/stub';
import Operation from 'app/operation';

import { injectable } from 'inversify';
import TYPES from 'core/types';

// Interfaces
import PresentationLayer from 'core/interfaces/presentationLayer';
import { Log } from 'core/interfaces/log';
import ApplicationError from 'core/interfaces/applicationError';

// Enums
import ResultType from 'core/enums/resultType';
import Server from 'presentation/rest/server';

import { CreateSurveyResult } from 'app/survey/create';
import { CreateSurveyResponse } from 'core/interfaces/survey/create';

export const url = '/survey';

export const mockPayload = {
    name: "Vzgo",
    email: "alex@example.com",
    age: 33,
    gender: "female",
    country: "USA",
    experience: 3,
};

export const error = {
    code: 100,
    message:
        'Unexpected error: if the error persists, please contact an administrator, '
        + 'quoting the code and timestamp of this error',
};

@injectable()
class MockCreate implements Operation<
    ResultType,
    CreateSurveyResponse,
    ApplicationError
> {
    public async execute(): Promise<CreateSurveyResult> {
        return {
            resultType: ResultType.UnexpectedError,
        };
    }
}

export const setup = async (): Promise<void> => {
    container.snapshot();

    container
        .rebind<PresentationLayer>(TYPES.PresentationLayer)
        .to(Server)
        .inSingletonScope();

    process.env.RUNTIME_ENV = 'testing';

    container
        .rebind<Log>(TYPES.Log)
        .to(StubLog)
        .inSingletonScope();

    container
        .rebind<MockCreate>(TYPES.CreateSurvey)
        .to(MockCreate);
};

export const restore = (): void => {
    container.restore();
};
