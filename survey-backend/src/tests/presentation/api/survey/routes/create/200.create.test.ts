import * as chai from 'chai';
import 'mocha';

import container from 'container';
import TYPES from 'core/types';

import Server from 'presentation/rest/server';

import * as fixture from './200.create.fixture';

const { expect } = chai;

describe('presentation - rest - survey - create - 200', () => {
    beforeEach(fixture.setup);
    afterEach(fixture.restore);

    it('successfully creates survey', async () => {
        const presentationLayer: Server = container.get<Server>(
            TYPES.PresentationLayer,
        );
        await presentationLayer.register();
        await presentationLayer.mount();

        const response = await presentationLayer.server.inject({
            method: 'POST',
            url: fixture.url,
            payload: fixture.mockPayload,
        });

        expect(response.statusCode).to.equal(200);

        expect(response).to.have.property('payload');

        const responsePayload = JSON.parse(response.payload);

        expect(responsePayload).to.deep.equal(fixture.mockCreateSurveyResponse);
    });
});
