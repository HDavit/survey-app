import { Log } from 'core/interfaces/log';
import TYPES from 'core/types';

import container from 'container';

import Application from 'app/application';

const app = container.get<Application>(TYPES.Application);
const log = container.get<Log>(TYPES.Log);

app.start()
    .catch((error) => {
        log.error('Error while starting application', error);
        throw error;
    });
