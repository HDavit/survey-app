const nodeExternals = require('webpack-node-externals');
const NodemonPlugin = require('nodemon-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const BannerPlugin = require('webpack').BannerPlugin;

module.exports = {
    devtool: 'source-map',
    entry: './index.ts',
    externals: [nodeExternals()],
    mode: process.env.RUNTIME_ENV !== 'development' ? 'production' : 'development',
    module: {
        rules: [
            {
                loader: 'ts-loader',
                test: /\.ts$/,
            },
        ],
    },
    output: {
        filename: 'index.js',
        path: __dirname + '/dist',
    },
    plugins: [
        new BannerPlugin({
            banner: 'require("source-map-support").install();',
            raw: true,
            entryOnly: false,
        }),
        new NodemonPlugin({
            nodeArgs: [
                '--inspect=0.0.0.0:9229',
            ],
        }),
        new CopyPlugin({
            patterns: [
                { from: './assets/docs/index.html', to: '' },
            ]
        }),
    ],
    resolve: {
        extensions: ['.ts', '.js'],
        modules: [
            __dirname,
            __dirname + '/src',
            __dirname + '/node_modules',
        ],
    },
    target: 'node',
};
